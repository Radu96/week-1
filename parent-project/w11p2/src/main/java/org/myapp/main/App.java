package org.myapp.main;

import org.myapp.w11p2.ThreadRace;
import org.myapp.w11p2.ThreadRaceCompetitor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App
{
    private static final int NUMBER_OF_THREADS = 10;

    public static void main( String[] args )
    {
        List <ThreadRaceCompetitor> threadRaceCompetitors = new ArrayList<>();
        ThreadRace threadRace = new ThreadRace(threadRaceCompetitors);

        ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

        threadRace.createInstances();

        for(int i = 0; i < threadRace.getThreadRaceCompetitors().size(); i++) {
            executorService.execute(threadRace);
        }

        executorService.shutdown();
    }
}
