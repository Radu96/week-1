package org.myapp.w11p2;

import java.util.List;

public class ThreadRace implements Runnable {
    private List <ThreadRaceCompetitor> threadRaceCompetitors;
    private static final int NUMBER_OF_INSTANCES = 10;

    public ThreadRace(List<ThreadRaceCompetitor> threadRaceCompetitors) {
        this.threadRaceCompetitors = threadRaceCompetitors;
    }

    public void createInstances() {
        for(int i = 0; i < NUMBER_OF_INSTANCES; i++) {
            threadRaceCompetitors.add(new ThreadRaceCompetitor());
        }
    }

    @Override
    public void run() {
        System.out.println("Running thread race competitor, with nano seconds: " + System.nanoTime());
    }

    public List<ThreadRaceCompetitor> getThreadRaceCompetitors() {
        return threadRaceCompetitors;
    }
}
