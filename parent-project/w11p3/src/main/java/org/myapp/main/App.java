package org.myapp.main;

import org.myapp.w11p3.ThreadRelayRace;
import org.myapp.w11p3.ThreadRelayRaceTeam;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App
{
    private static final int NUMBER_OF_THREADS = 10;

    public static void main( String[] args )
    {
        List <ThreadRelayRaceTeam> threadRelayRaceTeams = new ArrayList<>();
        ThreadRelayRace threadRelayRace = new ThreadRelayRace(threadRelayRaceTeams);

        ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

        threadRelayRace.createInstances();

        for(int i = 0; i < threadRelayRace.getThreadRelayRaceTeams().size(); i++) {
            executorService.execute(threadRelayRace);
        }

        executorService.shutdown();
    }
}
