package org.myapp.w11p3;

import java.util.List;

public class ThreadRelayRace implements Runnable {
    private List <ThreadRelayRaceTeam> threadRelayRaceTeams;
    private static final int NUMBER_OF_INSTANCES = 10;

    public ThreadRelayRace(List<ThreadRelayRaceTeam> threadRelayRaceTeams) {
        this.threadRelayRaceTeams = threadRelayRaceTeams;
    }

    public void createInstances() {
        for(int i = 0; i < NUMBER_OF_INSTANCES; i++) {
            threadRelayRaceTeams.add(new ThreadRelayRaceTeam());
        }
    }

    @Override
    public void run() {
        System.out.println("Running thread relay race team, with nano seconds: " + System.nanoTime());
    }

    public List<ThreadRelayRaceTeam> getThreadRelayRaceTeams() {
        return threadRelayRaceTeams;
    }
}
