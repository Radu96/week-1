package w1p4.myApp;

import java.util.ArrayList;
import java.util.List;

public class ConnectionManager {

    public class Connection {
        private Connection() {}
    }

    private ConnectionManager() {}

    private static ConnectionManager connectionManager = new ConnectionManager();
    private List <Connection> connections = new ArrayList<>();
    final int FIRST_POSITION = 0;

    public static ConnectionManager getInstance() {
        return connectionManager;
    }

    public void createObjects(int number) {
        final int MAXIMUM_SIZE = 5;

        for(int i = 0; i < number; i++)
        {
            if (connections.size() == MAXIMUM_SIZE)
            {
                printObjects();
                System.out.println("Maximum size exceeded, exiting...");
                System.exit(0);
            }
            connections.add(new Connection());
        }
    }

    public void deleteObject() {
        connections.remove(FIRST_POSITION);
        System.out.println("Deleting first object...");
    }

    public Connection getObject(int index) {
        if(connections.isEmpty())
            return null;

        return connections.get(index);
    }

    public void printObjects() {
        if(getObject(FIRST_POSITION) == null)
            System.out.println("List is empty");
        else
            for(int i = 0; i < connections.size(); i++)
                System.out.println(getObject(i));
    }
}

