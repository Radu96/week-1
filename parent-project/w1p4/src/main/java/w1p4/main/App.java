package w1p4.main;

import w1p4.myApp.ConnectionManager;

import java.util.InputMismatchException;
import java.util.Scanner;

public class App
{
    public static void main( String[] args )
    {
    	int number;
    	Scanner scanner = new Scanner(System.in);
		ConnectionManager connectionManager = ConnectionManager.getInstance();

    	try {
			System.out.print("How many objects? ");
    		number = scanner.nextInt();
    		if(number < 1)
			{
				System.err.println("The number must be greater than 0");
				System.exit(-1);
			}
	    	connectionManager.createObjects(number);
    	}
    	catch(InputMismatchException ex) {
    		System.err.println("Please enter only positive numbers");
    		System.exit(-1);
    	}

    	connectionManager.printObjects();

    	try {
	    	connectionManager.deleteObject();
			connectionManager.deleteObject();
			connectionManager.deleteObject();
    	}
    	catch(IndexOutOfBoundsException ex) {
    		System.err.println("Error: Index out of bounds");
    		System.exit(-1);
    	}

		//connectionManager.createObjects(6); //for testing purposes
		connectionManager.printObjects();

    }
}
