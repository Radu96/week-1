package org.myApp.w9p1;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class MyCollectionTest {

    private MyClass <Integer> integerMyClass;
    private MyClass <String> stringMyClass;

    @Before
    public void setUp() {
        List <Integer> integers = new ArrayList<>();
        List <String> strings = new ArrayList<>();

        integers.add(12);
        integers.add(472);
        integers.add(3481);
        integers.add(968);

        strings.add("string1");
        strings.add("string2");
        strings.add("string3");
        strings.add("string4");

        integerMyClass = new MyClass<>(integers);
        stringMyClass = new MyClass<>(strings);
    }

    @Test
    public void testContainsAllMethodWithIntegerList() {
        List <Integer> integerList = Arrays.asList(472, 12, 968);

        assertTrue(integerMyClass.containsAll(integerList));
    }

    @Test
    public void testContainsAllMethodWithStringList() {
        List <String> stringList = Arrays.asList("string2", "string4", "string1");

        assertTrue(stringMyClass.containsAll(stringList));
    }

    @Test
    public void testAddAllMethodWithIntegerList() {
        List <Integer> integerList = Arrays.asList(23, 34, 45);

        assertTrue(integerMyClass.addAll(integerList));
    }

    @Test
    public void testAddAllMethodWithStringList() {
        List <String> stringList = Arrays.asList("string10", "string20", "string30");

        assertTrue(stringMyClass.addAll(stringList));
    }
}
