package org.myApp.w9p1;

import java.util.Collection;

public class MyClass<T> implements MyCollection<T> {

    private Collection<T> collection;

    public MyClass(Collection<T> collection) {
        this.collection = collection;
    }

    @Override
    public boolean containsAll(Collection<T> c) {
        return collection.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<T> c) {
        return collection.addAll(c);
    }
}
