package org.myApp.main;

import org.myApp.w9p1.MyClass;

import java.util.ArrayList;
import java.util.List;

public class App
{
    public static void main( String[] args )
    {
        List <Integer> integers = new ArrayList<>();
        List <String> strings = new ArrayList<>();

        MyClass <Integer> integerMyClass = new MyClass<>(integers);
        MyClass <String> stringMyClass = new MyClass<>(strings);

        integers.add(12);
        integers.add(472);
        integers.add(3481);

        strings.add("string1");
        strings.add("string2");
        strings.add("string3");

        boolean containsAllForIntegers = integerMyClass.containsAll(integers);
        boolean addAllForIntegers = integerMyClass.addAll(integers);

        boolean containsAllForStrings = stringMyClass.containsAll(strings);
        boolean addAllForStrings = stringMyClass.addAll(strings);

        System.out.println("Return value of containsAll() for Integers: " + containsAllForIntegers);
        System.out.println("Return value of addAll() for Integers: " + addAllForIntegers);
        System.out.println("Return value of containsAll() for Strings: " + containsAllForStrings);
        System.out.println("Return value of addAll() for Strings: " + addAllForStrings);
    }
}
