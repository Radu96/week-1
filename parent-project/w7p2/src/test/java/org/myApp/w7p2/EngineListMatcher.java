package org.myApp.w7p2;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.myApp.w7p2.domain.Engine;

import java.util.List;

public class EngineListMatcher extends TypeSafeMatcher<List<Engine>> {

    private List <Engine> engineList;
    private final static int FIRST_POSITION = 0;

    public EngineListMatcher(List<Engine> engines) {
        this.engineList = engines;
    }

    @Override
    protected boolean matchesSafely(List<Engine> expectedEngineList) {
        return  engineList.size() == expectedEngineList.size() &&
                engineList.get(FIRST_POSITION).getEngineArchitecture() == expectedEngineList.get(FIRST_POSITION).getEngineArchitecture() &&
                engineList.get(FIRST_POSITION).getDisplacement() == expectedEngineList.get(FIRST_POSITION).getDisplacement() &&
                engineList.get(FIRST_POSITION).getHorsePower() == expectedEngineList.get(FIRST_POSITION).getHorsePower();
    }

    @Override
    public void describeTo(Description description) {

    }
}
