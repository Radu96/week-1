package org.myApp.w7p2;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.myApp.w7p2.domain.Employee;
import org.myApp.w7p2.domain.Engine;
import org.myApp.w7p2.domain.EngineArchitecture;
import org.myApp.w7p2.exception.UnqualifiedEmployeeException;
import org.myApp.w7p2.factory.EngineFactory;
import org.myApp.w7p2.service.EmployeeService;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class EngineFactoryTest {

    private List <Engine> engines;
    private EngineFactory engineFactory;

    private EmployeeService employeeServiceMock = Mockito.mock(EmployeeService.class);

    private Employee firstEmployee;
    private Employee secondEmployee;
    private Employee thirdEmployee;
    private Employee fourthEmployee;

    @Before
    public void setUp() {
        engines = new ArrayList<>();
        engineFactory = new EngineFactory(employeeServiceMock);

        firstEmployee = new Employee("Daniel");
        secondEmployee = new Employee("John");
        thirdEmployee = new Employee("Wayne");
        fourthEmployee = new Employee("Jason");
    }

    @Test(expected = UnqualifiedEmployeeException.class)
    public void testForUnqualifiedEmployeeShouldThrowException() {
        int numberOfEngines = 1;

        when(employeeServiceMock.isAssemblyLineWorker(firstEmployee)).thenThrow(UnqualifiedEmployeeException.class);

        engineFactory.manufactureEngines(numberOfEngines, firstEmployee);
    }

    @Test
    public void testForManufactureEngines() {
        int numberOfEngines = 1;

        when(employeeServiceMock.isAssemblyLineWorker(firstEmployee)).thenReturn(true);

        engineFactory.manufactureEngines(numberOfEngines, firstEmployee);
    }

    @Test
    public void testForEqualEngineLists() {
        int numberOfEngines = 3;
        double displacement = 2.0;
        int horsePower = 210;

        when(employeeServiceMock.isAssemblyLineWorker(secondEmployee)).thenReturn(true);

        List <Engine> engines = engineFactory.manufactureEngines(numberOfEngines, secondEmployee);
        List <Engine> engines1 = new ArrayList<>();

        for (int i = 0; i < numberOfEngines; i++) {
            engines1.add(new Engine(EngineArchitecture.L4, displacement, horsePower));
        }

        EngineListMatcher engineListMatcher = new EngineListMatcher(engines1);

        assertThat(engines, is(engineListMatcher));
    }

    @Test
    public void testForAssemblyLineWorker() {
        when(employeeServiceMock.isAssemblyLineWorker(thirdEmployee)).thenReturn(true);

        assertThat(employeeServiceMock.isAssemblyLineWorker(thirdEmployee), is(true));
    }

    @Test
    public void testForAdministrator() {
        when(employeeServiceMock.isAdministrator(fourthEmployee)).thenReturn(true);

        assertThat(employeeServiceMock.isAdministrator(fourthEmployee), is(true));
    }
}
