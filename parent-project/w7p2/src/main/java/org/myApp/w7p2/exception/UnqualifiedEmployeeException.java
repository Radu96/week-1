package org.myApp.w7p2.exception;

public class UnqualifiedEmployeeException extends RuntimeException {

    public UnqualifiedEmployeeException(String message) {
        super(message);
    }
    
}
