package org.myApp.w7p2.exception;

public class UnauthorizedEmployeeException extends RuntimeException {

    public UnauthorizedEmployeeException(String message) {
        super(message);
    }
    
}
