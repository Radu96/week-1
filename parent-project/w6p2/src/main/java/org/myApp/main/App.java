package org.myApp.main;

import org.myApp.w6p2.*;

public class App
{
    public static void main( String[] args )
    {
        try {
            Object object = LoggingProxyFactory.create(Human.class, new Person());
            Human human = (Human) object;

            human.walk();
            human.talk();
            human.eat();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
