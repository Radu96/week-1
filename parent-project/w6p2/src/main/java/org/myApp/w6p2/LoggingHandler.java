package org.myApp.w6p2;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.logging.Logger;

public class LoggingHandler implements InvocationHandler {
    private Object object;
    private static Logger logger = Logger.getLogger(LoggingHandler.class.getName());

    public LoggingHandler(Object object) {
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();
        Class<?> classObject = object.getClass();

        try {
            if (checkMethodAnnotation(methodName))
                logger.info("Method " + methodName + "() has @Logged annotation");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        if (checkClassAnnotation(classObject))
            logger.info("Class " + classObject.getSimpleName() + " has @Logged annotation");

        return method.invoke(object, args);
    }

    private boolean checkMethodAnnotation(String methodName) throws NoSuchMethodException {
        Method method = object.getClass().getMethod(methodName);
        Logged methodAnnotation = method.getAnnotation(Logged.class);

        return methodAnnotation != null;
    }

    private boolean checkClassAnnotation(Class<?> classObject) {
        Logged classAnnotation = classObject.getAnnotation(Logged.class);

        return classAnnotation != null;
    }
}
