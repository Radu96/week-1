package org.myApp.w6p2;

@Logged
public class Person implements Human {

    @Override
    public void walk() {
        System.out.println("Person is walking");
    }

    @Logged
    @Override
    public void talk() {
        System.out.println("Person is talking");
    }

    @Logged
    @Override
    public void eat() {
        System.out.println("Person is eating");
    }
}
