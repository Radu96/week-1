package org.myApp.w6p2;

public interface Human {
    void walk();
    void talk();
    void eat();
}
