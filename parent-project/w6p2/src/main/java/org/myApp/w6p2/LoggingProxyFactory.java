package org.myApp.w6p2;

import java.lang.reflect.Proxy;

public class LoggingProxyFactory {
    private LoggingProxyFactory(){}

    public static Object create(Class objectInterface, Object object) throws Exception {
        LoggingHandler loggingHandler = new LoggingHandler(object);

        return( Proxy.newProxyInstance(
                        object.getClass().getClassLoader(),
                        new Class[]{objectInterface},
                        loggingHandler)
        );
    }
}
