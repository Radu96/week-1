package org.myApp.main;

import org.myApp.w5p3.RandomWords;

public class App
{
    public static void main( String[] args )
    {
        RandomWords randomWords = new RandomWords();

        System.out.println("");
        randomWords.printSentences();

        System.out.println("\n--- Story ---");
        randomWords.printStory();
    }
}
