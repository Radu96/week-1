package org.myApp.w5p3;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomWords {
    private String[] article = {"the", "a", "one", "some", "any"};
    private String[] noun = {"boy", "girl", "dog", "town", "car"};
    private String[] verb = {"drove", "jumped", "ran", "walked", "skipped"};
    private String[] preposition = {"to", "from", "over", "under", "on"};
    private List <String> storySentences = new ArrayList<>();
    private Random random = new Random();

    public void printSentences() {
        String sentence, newSentence;
        int numberOfSentences = 20;
        int numberOfSentencesInTheStory = 5;
        int storyIndex = 0;

        for(int index = 0; index < numberOfSentences; index++) {
            sentence = concatWords();
            newSentence = capitalizeFirstLetter(sentence);

            if(storyIndex < numberOfSentencesInTheStory) {
                storySentences.add(newSentence);
                storyIndex++;
            }

            System.out.println(newSentence);
        }

    }

    public void printStory() {
        for (String storySentence : storySentences)
            System.out.println(storySentence);
    }

    private String concatWords() {
        int bound = 5;
        String sentence;

        sentence = article[random.nextInt(bound)] + " " + noun[random.nextInt(bound)] + " " +
                verb[random.nextInt(bound)] + " " + preposition[random.nextInt(bound)] + " " +
                article[random.nextInt(bound)] + " " + noun[random.nextInt(bound)] + ".";

        return sentence;
    }

    private String capitalizeFirstLetter(String sentence) {
        int firstBeginIndex = 0, endIndex = 1, secondBeginIndex = 1;
        String newSentence;

        newSentence = sentence.substring(firstBeginIndex, endIndex).toUpperCase() + sentence.substring(secondBeginIndex);

        return newSentence;
    }
}
