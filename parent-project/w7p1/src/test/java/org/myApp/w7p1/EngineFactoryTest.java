package org.myApp.w7p1;

import org.junit.Before;
import org.junit.Test;
import org.myApp.w7p1.domain.Employee;
import org.myApp.w7p1.domain.Engine;
import org.myApp.w7p1.domain.EngineComponent;
import org.myApp.w7p1.exception.InsufficientStockException;
import org.myApp.w7p1.exception.UnauthorizedEmployeeException;
import org.myApp.w7p1.exception.UnqualifiedEmployeeException;
import org.myApp.w7p1.factory.EngineFactory;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class EngineFactoryTest {

    private static final int COMPONENTS_PER_ENGINE = 3;
    private List <Employee> employees;
    private List <EngineComponent> engineComponents;
    private List <Engine> engines;
    private EngineFactory engineFactory;

    private Employee firstEmployee;
    private Employee secondEmployee;
    private Employee thirdEmployee;
    private Employee fourthEmployee;

    private EngineComponent piston;
    private EngineComponent valve;
    private EngineComponent crankshaft;
    private EngineComponent camshaft;

    @Before
    public void setUp() {
        employees = new ArrayList<>();
        engineComponents = new ArrayList<>();
        engines = new ArrayList<>();

        firstEmployee = new Employee("Daniel", true, true);
        secondEmployee = new Employee("John", false, true);
        thirdEmployee = new Employee("Wayne", false, false);
        fourthEmployee = new Employee("Jason", false, false);

        piston = new EngineComponent("piston", 10.12);
        valve = new EngineComponent("valve", 5.7);
        crankshaft = new EngineComponent("crankshaft", 15.84);
        camshaft = new EngineComponent("camshaft", 12.36);

        employees.add(firstEmployee);
        employees.add(secondEmployee);
        employees.add(thirdEmployee);

        engineComponents.add(piston);
        engineComponents.add(valve);
        engineComponents.add(crankshaft);
        engineComponents.add(camshaft);

        engineFactory = new EngineFactory(employees, engineComponents);
    }

    @Test(expected = UnauthorizedEmployeeException.class)
    public void testForUnauthorizedEmployeeShouldThrowException() {
        int numberOfEngines = 3;

        engineFactory.manufactureEngines(numberOfEngines, fourthEmployee);
    }

    @Test(expected = UnqualifiedEmployeeException.class)
    public void testForUnqualifiedEmployeeShouldThrowException() {
        int numberOfEngines = 2;

        engineFactory.manufactureEngines(numberOfEngines, thirdEmployee);
    }

    @Test(expected = InsufficientStockException.class)
    public void testForInsufficientStockShouldThrowException() {
        int numberOfEngines = 4;

        engineFactory.manufactureEngines(numberOfEngines, firstEmployee);
    }

    @Test
    public void testManufactureEngine() {
        int numberOfEngines = 1;

        engines = engineFactory.manufactureEngines(numberOfEngines, firstEmployee);

        assertFalse(engines.isEmpty());
    }

    @Test
    public void testComponentsPerEngine() {
        assertEquals(COMPONENTS_PER_ENGINE, EngineFactory.getComponentsPerEngine());
    }

    @Test
    public void testEngineComponentWithMatcher() {
        EngineComponent engineComponent = new EngineComponent("piston", 10.12);
        EngineComponentMatcher engineComponentMatcher = new EngineComponentMatcher(engineComponent);

        assertThat(piston, is(engineComponentMatcher));
    }
}
