package org.myApp.w7p1;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.myApp.w7p1.domain.EngineComponent;

public class EngineComponentMatcher extends TypeSafeMatcher<EngineComponent> {

    private EngineComponent engineComponent;

    public EngineComponentMatcher(EngineComponent engineComponent) {
        this.engineComponent = engineComponent;
    }

    @Override
    protected boolean matchesSafely(EngineComponent expectedEngineComponent) {
        return engineComponent.getName().equalsIgnoreCase(expectedEngineComponent.getName()) &&
                engineComponent.getWeight() == expectedEngineComponent.getWeight();
    }

    @Override
    public void describeTo(Description description) {
    }
}
