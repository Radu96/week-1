package org.myApp.w7p1.domain;

public enum EngineArchitecture {
    L4, L6, V6, V8, V10, V12
}
