package w2p3.main;

import w2p3.myApp.*;

public class App
{
    public static void main( String[] args )
    {
        Animal dog = new Dog("Pit Bull", 5, 25, true);
        Animal cat = new Cat("Persian cat", 7, 6, false);
        Animal horse = new Horse("Arabian horse", 10, 400, true);

        dog.makeSound();
        cat.makeSound();
        horse.makeSound();

        System.out.println("Value of variable count after execution: " + Animal.getCount());
    }
}
