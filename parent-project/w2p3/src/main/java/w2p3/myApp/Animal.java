package w2p3.myApp;

public class Animal {
    protected String breed;
    protected int age, mass;
    protected boolean hasMaster;
    private static int count;

    {
        System.out.println("Animal class: Initializer block called");
    }

    public Animal(String breed, int age, int mass, boolean hasMaster) {
        this.breed = breed;
        this.age = age;
        this.mass = mass;
        this.hasMaster = hasMaster;
        count++;
        System.out.println("Animal class: constructor called");
    }

    static {
        count = 0;
        System.out.println("Initial value of variable count: " + count);
        //System.out.println("Animal class: Instantiating Cat object in static block");
        //Animal cat = new Cat("Savannah", 4, 7, true);
    }

    public static int getCount() {
        return count;
    }

    public void makeSound() {
        System.out.println("Animal makes sound");
    }
}
