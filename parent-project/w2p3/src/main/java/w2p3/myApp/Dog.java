package w2p3.myApp;

public class Dog extends Animal {

    static {
        System.out.println("Dog class: Instantiating Horse object in static block");
        Animal horse = new Horse("Friesian horse", 10, 400, true);
        horse.makeSound();
    }

    public Dog(String breed, int age, int mass, boolean hasMaster) {
        super(breed, age, mass, hasMaster);
        System.out.println("Dog class: object created");
    }

    @Override
    public void makeSound() {
        System.out.println(this.breed + " makes sound");
    }
}
