package w2p3.myApp;

public class Horse extends Animal {

    public Horse(String breed, int age, int mass, boolean hasMaster) {
        super(breed, age, mass, hasMaster);
        System.out.println("Horse class: object created");
    }

    @Override
    public void makeSound() {
        System.out.println(this.breed + " makes sound");
    }
}
