package w2p3.myApp;

public class Cat extends Animal {

    {
        System.out.println("Cat class: Instantiating Dog object in initializer block");
        Animal dog = new Dog("Rottweiler", 8, 55, false);
        dog.makeSound();
    }

    public Cat(String breed, int age, int mass, boolean hasMaster) {
        super(breed, age, mass, hasMaster);
        System.out.println("Cat class: object created");
    }

    @Override
    public void makeSound() {
        System.out.println(this.breed + " makes sound");
    }
}
