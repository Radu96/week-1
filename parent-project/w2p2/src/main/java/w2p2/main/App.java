package w2p2.main;

import w2p2.myApp.Tank;

import java.util.InputMismatchException;
import java.util.Scanner;

public class App
{
    public static void main( String[] args )
    {
        Tank tank;
        Scanner scanner = new Scanner(System.in);
        int number = 0, number1 = 0, numberOfDeletions = 3;

        System.out.print("How many numbers in the stack? ");

        try {
            number = scanner.nextInt();
            if(number < 1)
            {
                System.err.println("The size must be greater than 0");
                System.exit(-1);
            }
        }
        catch(InputMismatchException ex) {
            System.err.println("Please enter only positive numbers");
            System.exit(-1);
        }

        tank = new Tank(number);

        for(int i = 0; i < number; i++)
        {
            System.out.print("Enter number: ");
            try {
                number1 = scanner.nextInt();
            }
            catch(InputMismatchException ex) {
                System.err.println("Please enter only positive numbers");
                System.exit(-1);
            }

            tank.push(number1);
        }

        //tank.push(23); //for testing purposes

        for (int i = 0; i < numberOfDeletions; i++) {
            tank.pop();
            if(tank.isEmpty())
            {
                System.out.println("All the numbers were removed from stack");
                System.out.println("Stack is empty");
                tank = null;
                System.gc();
                System.exit(1);
            }
        }

        System.out.println(numberOfDeletions + " numbers removed from stack");
        System.out.println("Stack is not empty");
    }
}