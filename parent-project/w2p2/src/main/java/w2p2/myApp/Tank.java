package w2p2.myApp;

public class Tank
{
    private int[] array;
    private int top;

    public Tank(int size) {
        array = new int[size];
        top = -1;
    }

    public int push(int number) {
        if((top+1) == array.length)
        {
            System.out.println("Stack overflow! Exiting..");
            System.exit(-1);
        }

        array[++top] = number;
        return array[top];
    }

    public int pop() {
        if(isEmpty()) {
            System.out.println("Stack is empty");
            System.exit(1);
        }

        return array[top--];
    }

    public boolean isEmpty() {
        return top == -1;
    }

    @Override
    public void finalize() {
        System.out.println("Object is cleaned up");
    }
}