package w3p3.myApp;

public class Line implements Shape {

    private Point firstPoint = new Point();
    private Point secondPoint = new Point();

    public Line() {
        System.out.println("Line class: object created");
    }

    public void draw() {
        System.out.println("Drawing a line");
    }
}
