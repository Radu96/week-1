package w3p3.myApp;

public class Point implements Shape {

    int xCoordinate, yCoordinate;

    public Point() {
        System.out.println("Point class: object created");
    }

    public void draw() {
        System.out.println("Drawing a point");
    }
}
