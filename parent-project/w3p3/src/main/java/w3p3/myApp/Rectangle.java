package w3p3.myApp;

public class Rectangle implements Shape {

//    Point firstPoint = new Point();
//    Point secondPoint = new Point();
//    Point thirdPoint = new Point();
//    Point fourthPoint = new Point();

    private Line firstLine = new Line();
    private Line secondLine = new Line();
    private Line thirdLine = new Line();
    private Line fourthLine = new Line();

    public Rectangle() {
        System.out.println("Rectangle class: object created");
    }

    public void draw() {
        System.out.println("Drawing a rectangle");
    }
}
