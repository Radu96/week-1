package w3p3.myApp;

public class Circle implements Shape {

    //Point point = new Point();
    private Line line = new Line();

    public Circle() {
        System.out.println("Circle class: object created");
    }

    public void draw() {
        System.out.println("Drawing a circle");
    }
}
