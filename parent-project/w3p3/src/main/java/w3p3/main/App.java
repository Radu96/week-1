package w3p3.main;

import w3p3.myApp.*;

public class App
{
    public static void main( String[] args )
    {
        Rectangle rectangle = new Rectangle();
        Circle circle = new Circle();

        rectangle.draw();
        circle.draw();
    }
}