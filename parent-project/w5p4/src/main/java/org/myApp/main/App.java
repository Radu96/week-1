package org.myApp.main;

import org.myApp.w5p4.PigLatin;

import java.util.Scanner;

public class App
{
    public static void main( String[] args )
    {
        String sentence;
        Scanner scanner = new Scanner(System.in);
        PigLatin pigLatin = new PigLatin();

        System.out.println("");

        System.out.print("Enter the sentence: ");
        sentence = scanner.nextLine();

        System.out.print("Pig Latin sentence: ");
        pigLatin.tokenize(sentence);

        System.out.println("");
    }
}
