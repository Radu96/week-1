package org.myApp.w5p4;

import java.util.StringTokenizer;

public class PigLatin {

    private static final int FIRST_INDEX = 0;
    private static final int SECOND_INDEX = 1;

    public void tokenize(String sentence) {
        StringTokenizer stringTokenizer = new StringTokenizer(sentence);

        while(stringTokenizer.hasMoreTokens())
            printLatinWord(stringTokenizer.nextToken());

    }

    private void printLatinWord(String word) {
        String newWord, pigLatinWord;
        String pigLatinEnding = "ay";
        char firstCharacter = word.charAt(FIRST_INDEX);

        newWord = word.substring(SECOND_INDEX);
        pigLatinWord = newWord + firstCharacter + pigLatinEnding;

        System.out.print(pigLatinWord + " ");
    }

}
