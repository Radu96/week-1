package org.myApp.main;

import org.myApp.w6p3.Factory;
import org.myApp.w6p3.ObjectType;

public class App
{
    public static void main( String[] args )
    {
        Factory factoryObject = new Factory();
        Object object = factoryObject.returnObject(ObjectType.SUBCLASS);
    }
}
