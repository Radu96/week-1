package org.myApp.w6p3;

import org.myApp.classLoader.CustomClassLoader;

public class Factory {

    public MyClass returnObject(ObjectType objectType) {
        switch (objectType) {
            case ORIGINAL: return new MyClass();
            case RELOADED: return (MyClass) createObject(objectType.getClassPath(), ObjectType.RELOADED);
            case SUBCLASS: return (MyClass) createObject(objectType.getClassPath(), ObjectType.SUBCLASS);
        }

        return null;
    }

    private Object createObject(String className, ObjectType objectType) {
        ClassLoader parentClassLoader = CustomClassLoader.class.getClassLoader();
        CustomClassLoader customClassLoader = new CustomClassLoader(parentClassLoader, objectType);
        Object object = null;
        Class objectClass;

        try {
            objectClass = customClassLoader.findClass(className);
            object = objectClass.newInstance();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return object;
    }
}
