package org.myApp.w6p3;

public enum ObjectType {
    ORIGINAL("MyClass.class", "org.myApp.w6p3.MyClass"),
    RELOADED("MyClass.class", "org.myApp.w6p3.MyClass"),
    SUBCLASS("SubClass.class", "org.myApp.w6p3.SubClass");

    private String classFileName;
    private String classPath;

    ObjectType(String classFileName, String classPath) {
        this.classFileName = classFileName;
        this.classPath = classPath;
    }

    public String getClassFileName() {
        return classFileName;
    }

    public String getClassPath() {
        return classPath;
    }
}
