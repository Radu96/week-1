package org.myApp.classLoader;

import org.myApp.w6p3.ObjectType;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class CustomClassLoader extends ClassLoader {

    private ObjectType objectType;

    public CustomClassLoader(ClassLoader parent, ObjectType objectType) {
        super(parent);
        this.objectType = objectType;
    }

    @Override
    public Class findClass(String classFileName) throws ClassNotFoundException {
        classFileName = objectType.getClassFileName();
        byte[] bytes = loadClassFromResources(classFileName);
        int offset = 0;

        return defineClass(objectType.getClassPath(), bytes, offset, bytes.length);
    }

    private byte[] loadClassFromResources(String classFileName) {
        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(classFileName);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bytes;
        int nextValue;

        try {
            while ( (nextValue = inputStream.read()) != -1 ) {
                byteArrayOutputStream.write(nextValue);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        bytes = byteArrayOutputStream.toByteArray();

        return bytes;
    }
}
