package org.myApp.w6p3;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ClassTest {
    Factory factoryObject;

    @Before
    public void setUp() {
        factoryObject = new Factory();
    }

    @Test(expected = ClassCastException.class)
    public void testForReloadedObjectTypeShouldThrowException() {
        factoryObject.returnObject(ObjectType.RELOADED);
    }

    @Test
    public void testForOriginalObjectType() {
        if(factoryObject.returnObject(ObjectType.ORIGINAL) != null)
            assertTrue(true);
    }

    @Test
    public void testForSubClassObjectType() {
        if(factoryObject.returnObject(ObjectType.SUBCLASS) != null)
            assertTrue(true);
    }
}
