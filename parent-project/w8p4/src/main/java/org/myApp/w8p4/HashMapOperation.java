package org.myApp.w8p4;

import java.util.*;

public class HashMapOperation {
    private Map <Train, List<Integer>> myHashMap;
    private Random random;
    private static final int FIRST_INDEX = 0;
    private static final int LAST_DAY = 365;
    private static final int NUMBER_OF_RECORDS = 10000;

    public HashMapOperation(Map<Train, List<Integer>> myHashMap) {
        this.myHashMap = myHashMap;
    }

    public void addValuesToHashMap(int trainNumberBound, int numberOfWagonsBound, int numberOfRunningDays, int seed) {
        int randomTrainNumber;
        int randomDay;
        List <Integer> daysList;
        Set <Integer> daysSet;
        String[] trainTypes = {"type1", "type2", "type3", "type4", "type5"};
        random = new Random(seed);

        for(int i = FIRST_INDEX; i < NUMBER_OF_RECORDS; i++) {
            daysSet = new HashSet<>();

            while(daysSet.size() != numberOfRunningDays) {
                randomDay = returnRandomDay();
                daysSet.add(randomDay);
            }

            daysList = new ArrayList<>(daysSet);

            randomTrainNumber = random.nextInt(trainNumberBound);

            if(randomTrainNumber == 0) {
                randomTrainNumber++;
            }

            Train train = new Train(randomTrainNumber, trainTypes[random.nextInt(trainTypes.length)], random.nextInt(numberOfWagonsBound));

            myHashMap.put(train, daysList);
        }
    }

    private int returnRandomDay() {
        int randomDay = random.nextInt(LAST_DAY + 1);

        if(randomDay == 0) {
            randomDay++;
        }

        return randomDay;
    }
}
