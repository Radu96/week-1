package org.myApp.w8p4;

import java.util.Objects;

public class Train {
    private int trainNumber;
    private String trainType;
    private int numberOfWagons;

    public Train(int trainNumber, String trainType, int numberOfWagons) {
        this.trainNumber = trainNumber;
        this.trainType = trainType;
        this.numberOfWagons = numberOfWagons;
    }

    @Override
    public int hashCode() {
        return Objects.hash(trainNumber, trainType, numberOfWagons);
    }
}
