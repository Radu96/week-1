package org.myApp.main;

import org.myApp.w8p4.HashMapOperation;
import org.myApp.w8p4.Train;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class App
{
    public static void main( String[] args )
    {
        Map <Train, List<Integer>> myHashMap = new HashMap<>();
        HashMapOperation hashMapOperation = new HashMapOperation(myHashMap);
        int trainNumberBound = 100;
        int numberOfWagonsBound = 11;
        int numberOfRunningDays = 20;
        int seed = 100;
        int count = 0;
        int numberLimit = 20;
        long startTime = System.nanoTime();

        hashMapOperation.addValuesToHashMap(trainNumberBound, numberOfWagonsBound, numberOfRunningDays, seed);

        for(Train train : myHashMap.keySet()) {
            System.out.println(myHashMap.get(train));
            count++;

            if(count == numberLimit) {
                break;
            }
        }

        long endTime = System.nanoTime();
        long elapsedTime = endTime - startTime;

        System.out.println();
        System.out.println("Elapsed time: " + elapsedTime + " nanoseconds");
    }
}
