package org.myApp.w8p2;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class DeckOfCardsTest {

    private List <Card> cards;

    @Before
    public void setUp() {
        cards = new ArrayList<>();
        DeckOfCards deckOfCards = new DeckOfCards(cards);

        deckOfCards.addObjectsToList();
    }

    @Test
    public void testForShuffledCards() {
        List <Card> shuffledCards = new ArrayList<>();
        DeckOfCards deckOfCards1 = new DeckOfCards(shuffledCards);
        long seed = 1000;
        boolean cardsAreShuffled = false;

        deckOfCards1.addObjectsToList();

        deckOfCards1.shuffleCards(seed);

        for(int i = 0; i < cards.size(); i++)
            if(!cards.get(i).equals(shuffledCards.get(i))) {
                cardsAreShuffled = true;
                break;
            }

        assertTrue(cardsAreShuffled);
    }
}
