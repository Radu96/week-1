package org.myApp.w8p2;

public class Card {
    private int number;
    private Suite suite;

    public Card(int number, Suite suite) {
        this.number = number;
        this.suite = suite;
    }

    public int getNumber() {
        return number;
    }

    public Suite getSuite() {
        return suite;
    }
}
