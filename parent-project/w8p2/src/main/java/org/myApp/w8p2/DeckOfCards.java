package org.myApp.w8p2;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class DeckOfCards {

    private List <Card> cards;
    private static final int FIRST_POSITION = 0;
    private static final int FIRST_NUMBER = 2;
    private static final int LAST_NUMBER = 14;

    public DeckOfCards(List<Card> cards) {
        this.cards = cards;
    }

    public void addObjectsToList() {
        for(int i = FIRST_NUMBER; i <= LAST_NUMBER; i++) {
            cards.add(new Card(i, Suite.HEART));
            cards.add(new Card(i, Suite.CLUB));
            cards.add(new Card(i, Suite.DIAMOND));
            cards.add(new Card(i, Suite.SPADE));
        }
    }

    public void shuffleCards(long seed) {
        Random random = new Random(seed);

        for (int i = cards.size() - 1; i > FIRST_POSITION; i--) {
            int index = random.nextInt(i + 1);

            Collections.swap(cards, i, index);
        }
    }
}
