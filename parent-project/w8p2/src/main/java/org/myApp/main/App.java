package org.myApp.main;

import org.myApp.w8p2.Card;
import org.myApp.w8p2.DeckOfCards;

import java.util.ArrayList;
import java.util.List;

public class App
{
    public static void main( String[] args )
    {
        List <Card> cards = new ArrayList<>();
        DeckOfCards deckOfCards = new DeckOfCards(cards);
        long seed = 1000;

        deckOfCards.addObjectsToList();

        deckOfCards.shuffleCards(seed);

        System.out.println("\nShuffled cards");

        for(Card card : cards)
            System.out.println(card.getNumber() + ", " + card.getSuite());

    }
}
