package w3p1.myApp;

public class Englishman extends Person {

    public Englishman(String firstName, String surname, String birthDate) {
        super(firstName, surname, birthDate);
    }

    public Englishman(String fullName, String birthDate) {
        super(fullName, birthDate);
    }

    @Override
    public String getBirthDate() {
        return birthDate;
    }

    @Override
    public String selfDescribe() {
        return "My name is " + firstName + " " + surname + " and I am " + calculateAge() + " years old";
    }
}
