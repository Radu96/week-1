package w3p1.myApp;

public class Frenchman extends Person {

    public Frenchman(String firstName, String surname, String birthDate) {
        super(firstName, surname, birthDate);
    }

    public Frenchman(String fullName, String birthDate) {
        super(fullName, birthDate);
    }

    @Override
    public String getBirthDate() {
        return birthDate;
    }

    @Override
    public String selfDescribe() {
        return "Mon nom est " + firstName + " " + surname + " et j'ai " + calculateAge() + " ans";
    }
}
