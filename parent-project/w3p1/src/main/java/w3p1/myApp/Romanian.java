package w3p1.myApp;

public class Romanian extends Person {

    public Romanian(String firstName, String surname, String birthDate) {
        super(firstName, surname, birthDate);
    }

    public Romanian(String fullName, String birthDate) {
        super(fullName, birthDate);
    }

    @Override
    public String getBirthDate() {
        return birthDate;
    }

    @Override
    public String selfDescribe() {
        return "Numele meu este " + firstName + " " + surname + " si am " + this.calculateAge() + " de ani";
    }

}
