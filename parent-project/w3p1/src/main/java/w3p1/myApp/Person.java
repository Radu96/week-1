package w3p1.myApp;

import java.util.Calendar;

public abstract class Person {
    protected String firstName = "";
    protected String surname = "";
    protected String birthDate;

    public Person(String firstName, String surname, String birthDate) {
        this.firstName = firstName;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Person(String fullName, String birthDate) {
        this.birthDate = birthDate;

        String[] stringArray = splitFullName(fullName);
        int surnamePosition = stringArray.length - 1;

        firstName = concatFirstName(stringArray);
        surname = stringArray[surnamePosition];
    }

    private String[] splitFullName(String fullName) {
        String regex = " ";
        return fullName.split(regex);
    }

    private String concatFirstName(String[] stringArray) {
        int lastFirstNamePosition = stringArray.length - 2;
        String firstName = "";

        for(int i = 0; i < lastFirstNamePosition; i++)
            firstName = firstName.concat(stringArray[i] + " ");

        firstName = firstName.concat(stringArray[lastFirstNamePosition]);
        return firstName;
    }

    public abstract String getBirthDate();

    public abstract String selfDescribe();

    protected int calculateAge() {
        String regex = "[ /.-]";
        String[] stringArray = getBirthDate().split(regex);
        int dayPosition = 0;
        int monthPosition = 1;
        int yearPosition = 2;

        int day = Integer.parseInt(stringArray[dayPosition]);
        int month = Integer.parseInt(stringArray[monthPosition]);
        int year = Integer.parseInt(stringArray[yearPosition]);

        int currentDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        if(currentMonth == month) {
            if (currentDay >= day)
                return currentYear - year;
            return currentYear - year - 1;
        }

        if(currentMonth > month)
            return currentYear - year;

        return currentYear - year - 1;
    }
}
