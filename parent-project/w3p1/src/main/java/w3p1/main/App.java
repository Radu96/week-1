package w3p1.main;

import w3p1.myApp.*;

public class App 
{
    public static void main( String[] args )
    {
        Person romanian = new Romanian("Andrei Marius Ionescu", "12/06/1990");
        Person englishman = new Englishman("Michael", "Johnson", "05-01-1992");
        Person frenchman = new Frenchman("Pierre Hugo", "Henry", "08.11.1988");

        System.out.println(romanian.selfDescribe());
        System.out.println(englishman.selfDescribe());
        System.out.println(frenchman.selfDescribe());
    }
}
