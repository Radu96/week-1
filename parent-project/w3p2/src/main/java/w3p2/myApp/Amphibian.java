package w3p2.myApp;

public class Amphibian {

    public void makeSound() {
        System.out.println("Amphibian makes sound");
    }

    public void move() {
        System.out.println("Amphibian is moving");
    }
}
