package w3p2.main;

import w3p2.myApp.*;

public class App 
{
    public static void main( String[] args )
    {
        Frog frog = new Frog();
        Amphibian upcastedFrog = (Amphibian) frog;

        upcastedFrog.makeSound();
        upcastedFrog.move();
    }
}
