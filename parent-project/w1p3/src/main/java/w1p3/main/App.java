package w1p3.main;

import w1p3.myApp.*;

public class App
{
    public static void main( String[] args )
    {
        Domain domain = new Domain("www.google.com", "Details", "ABC");
        Customer customer = new Customer("John");
        Reseller reseller = new Reseller("Scott");
        Reseller reseller1 = new Reseller("Andy");
        Registrar registrar = new Registrar();
        Registrar registrar1 = new Registrar();
        Registry registry = new Registry();

        System.out.println("");

        customer.addReseller(reseller);
        customer.addReseller(reseller1);

        reseller.addRegistrar(registrar);
        reseller.addRegistrar(registrar1);

        registrar.setRegistry(registry);
        registrar1.setRegistry(registry);

        customer.buy(domain);

    }
}
