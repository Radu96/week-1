package w1p3.myApp;

import java.util.ArrayList;
import java.util.List;

public class Customer {

    private String customerName;
    private List <Reseller> resellers = new ArrayList<>();

    public Customer(String customerName) {
        this.customerName = customerName;
    }

    public void buy(Domain domain) {
        final int FIRST_POSITION = 0;
        System.out.println("Customer " + this.customerName + " provides the following details:\n" +
                "Domain name: " + domain.getDomainName() +
                ", owner details: " + domain.getOwnerDetails() + ", hosts: " + domain.getHosts());

        resellers.get(FIRST_POSITION).buy(domain);
    }

    public void addReseller(Reseller reseller) {
        resellers.add(reseller);
    }

}
