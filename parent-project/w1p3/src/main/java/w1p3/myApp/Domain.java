package w1p3.myApp;

public class Domain {

    private String domainName, ownerDetails, hosts;

    public Domain(String domainName, String ownerDetails, String hosts) {
        this.domainName = domainName;
        this.ownerDetails = ownerDetails;
        this.hosts = hosts;
    }

    public String getDomainName() {
        return domainName;
    }

    public String getOwnerDetails() {
        return ownerDetails;
    }

    public String getHosts() {
        return hosts;
    }
}
