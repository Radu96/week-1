package w1p3.myApp;

import java.util.ArrayList;
import java.util.List;

public class Reseller {

    private String resellerName;
    private List <Registrar> registrars = new ArrayList<>();

    public Reseller(String resellerName) {
        this.resellerName = resellerName;
    }

    public void buy(Domain domain) {
        final int FIRST_POSITION = 0;
        boolean isDuplicate = registrars.get(FIRST_POSITION).createDomain(domain);

        if(!isDuplicate)
            System.out.println("Reseller " + this.resellerName + " buys domain");

        contactCustomer(isDuplicate);
    }

    public void addRegistrar(Registrar registrar) {
        registrars.add(registrar);
    }

    public void contactCustomer(boolean isDuplicate) {
        if(isDuplicate)
            System.out.println("Reseller contacts customer, informing that the domain name is taken");
        else
            System.out.println("Reseller contacts customer, informing that the domain name is available");
    }

}