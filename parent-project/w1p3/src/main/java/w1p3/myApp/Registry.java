package w1p3.myApp;

import java.util.ArrayList;
import java.util.List;

public class Registry {

    private List <Domain> domains = new ArrayList<>();

    public boolean checkDuplicates(Domain domain) {

        //adding the same domain, for testing purposes
//        Domain domainTest = new Domain("www.google.com", "Details", "ABC");
//        domains.add(domainTest);

        for (Domain value : domains)
            if (value.getDomainName().equals(domain.getDomainName()))
                return true;

            return false;
    }

    public void addDomainToDatabase(Domain domain) {
        domains.add(domain);
    }

}
