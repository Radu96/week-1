package w1p3.myApp;

public class Registrar {

    private Registry registry;

    public boolean createDomain(Domain domain) {

        if(registry.checkDuplicates(domain))
            return true;
        else {
            registry.addDomainToDatabase(domain);
            return false;
        }

    }

    public void setRegistry(Registry registry) {
        this.registry = registry;
    }

}
