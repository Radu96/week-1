package org.myApp.w9p2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class BubbleSortTest {

    private Integer[] integerArray;
    private String[] stringArray;
    private Double[] doubleArray;
    private BubbleSortGeneric bubbleSortGeneric;

    @Before
    public void setUp() {
        integerArray = new Integer[]{72, -962, 4327, -34871, 514};
        stringArray = new String[]{"horse", "cat", "zebra", "dog", "jaguar"};
        doubleArray = new Double[]{472.359, -83.95, -348.97, 8123.56, -4637.541};
        
        bubbleSortGeneric = new BubbleSortGeneric();
    }

    @Test
    public void testBubbleSortForIntegerArray() {
        Integer[] expectedIntegerArray = new Integer[]{-34871, -962, 72, 514, 4327};

        bubbleSortGeneric.performBubbleSortGeneric(integerArray);

        assertThat(integerArray, is(expectedIntegerArray));
    }

    @Test
    public void testBubbleSortForStringArray() {
        String[] expectedStringArray = new String[]{"cat", "dog", "horse", "jaguar", "zebra"};

        bubbleSortGeneric.performBubbleSortGeneric(stringArray);

        assertThat(stringArray, is(expectedStringArray));
    }

    @Test
    public void testBubbleSortForDoubleArray() {
        Double[] expectedDoubleArray = new Double[]{-4637.541, -348.97, -83.95, 472.359, 8123.56};

        bubbleSortGeneric.performBubbleSortGeneric(doubleArray);

        assertThat(doubleArray, is(expectedDoubleArray));
    }
}
