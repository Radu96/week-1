package org.myApp.w9p2;

public class BubbleSortGeneric {

    public <T extends Comparable<? super T>> void performBubbleSortGeneric(T[] array) {
        int arrayLength = array.length;
        boolean objectsWereSwapped;
        T temporaryElement;

        for(int i = 0; i < arrayLength - 1; i++) {
            objectsWereSwapped = false;

            for(int j = 0; j < arrayLength - i - 1; j++) {
                if (array[j].compareTo(array[j+1]) > 0) {
                    temporaryElement = array[j];
                    array[j] = array[j+1];
                    array[j+1] = temporaryElement;

                    objectsWereSwapped = true;
                }
            }

            if(!objectsWereSwapped)
                break;
        }
    }
}
