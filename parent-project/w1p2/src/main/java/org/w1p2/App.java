package org.w1p2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class App 
{
	public static boolean isPrime(int number) {

		for(int i = 2; i <= number/2; i++)
			if(number % i == 0)
				return false;
		
		return true;
	}
	
    public static void main( String[] args )
    {
       int number = 0, i;
       Scanner scanner = new Scanner(System.in);

       System.out.print("n = ");
       try {
    	   number = scanner.nextInt();
       } 
       catch(InputMismatchException ex) {
    	   System.err.println("Please enter only positive numbers");
    	   System.exit(-1);
       }
       
       if(number <= 1)
       {
    	   System.err.println("n must be greater than 1");
    	   System.exit(-1);
       }

       System.out.print(1 + ", ");
       for(i = 2; i < number; i++)
    	   if(isPrime(i))
    		   System.out.print(i + "-PRIME, ");
    	   else System.out.print(i + ", ");
       
       if(isPrime(number))
    	   System.out.print(number + "-PRIME");
       else System.out.print(number);
       
       scanner.close();
    }
}
