package org.myapp.main;

import org.myapp.w11p1.Client;
import org.myapp.w11p1.Cluster;
import org.myapp.w11p1.Node;
import org.myapp.w11p1.Server;

import java.util.ArrayList;
import java.util.List;

public class App
{
    public static void main( String[] args )
    {
        Server server = new Server();
        List <Node> nodes = new ArrayList<>();

        nodes.add(new Node("message1"));
        nodes.add(new Node("message2"));
        nodes.add(new Node("message3"));
        nodes.add(new Node("message4"));

        Cluster cluster = new Cluster(nodes);
        Node clientNode = new Node("message5");
        Client client = new Client(clientNode);

        Thread clusterThread = new Thread(cluster, "clusterThread");
        Thread clientThread = new Thread(client, "clientThread");

        for(Node node : nodes) {
            server.addMessageInQueue(node);
        }
    }
}
