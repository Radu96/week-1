package org.myapp.w11p1;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class Server {
    private Queue <String> messagesQueue;
    private static final int MAXIMUM_CAPACITY = 10;

    public Server() {
        messagesQueue = new ArrayBlockingQueue<>(MAXIMUM_CAPACITY);
    }

    public void addMessageInQueue(Node node) {
        if(messagesQueue.size() == MAXIMUM_CAPACITY)
            throw new IllegalStateException("Queue is full");

        System.out.println("Adding " + node.generateLoggingMessage() + " to queue");

        messagesQueue.add(node.generateLoggingMessage());
    }
}
