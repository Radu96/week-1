package org.myapp.w11p1;

public class Node {
    private String loggingMessage;

    public Node(String loggingMessage) {
        this.loggingMessage = loggingMessage;
    }

    public String generateLoggingMessage() {
        return loggingMessage;
    }
}
