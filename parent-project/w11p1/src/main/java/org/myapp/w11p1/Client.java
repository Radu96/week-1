package org.myapp.w11p1;

public class Client implements Runnable {
    private final Node node;

    public Client(Node node) {
        this.node = node;
    }

    @Override
    public void run() {
        String threadName = Thread.currentThread().getName();

        System.out.println(threadName + " started");

        synchronized (node) {
            try {
                System.out.println(threadName + " is waiting");
                node.wait();
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
