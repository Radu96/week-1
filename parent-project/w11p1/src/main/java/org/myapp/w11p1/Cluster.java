package org.myapp.w11p1;

import java.util.List;

public class Cluster implements Runnable {
    private List <Node> nodes;

    public Cluster(List<Node> nodes) {
        this.nodes = nodes;
    }

    @Override
    public void run() {
        String threadName = Thread.currentThread().getName();

        System.out.println(threadName + " started");

        for(Node node : nodes)
            synchronized (node) {
                System.out.println("Notifying thread");
                node.notify();
            }
    }
}
