package w4p1.myApp;

public class TaxiDriver implements Person, Driver {

    private String name;

    public TaxiDriver(String name) {
        this.name = name;
    }

    public void eat() {
        System.out.println("Driver " + name + " eats");
    }

    public void sleep() {
        System.out.println("Driver " + name + " sleeps");
    }

    public String getCurrentLocation() {
        return "mock location";
    }

    /**
     * Triggers depart to address
     *
     */
    public void goToAddress(String address) {
        int estimatedTimeOfArrival = 2;
        System.out.println("Driver " + name + " goes to " + address + ", with "
                + estimatedTimeOfArrival + " minutes estimated time of arrival");
    }

    public String getName() {
        return name;
    }

}
