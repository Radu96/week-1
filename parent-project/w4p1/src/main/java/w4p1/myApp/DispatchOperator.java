package w4p1.myApp;

import java.util.List;

public class DispatchOperator implements Person, Dispatcher {

    private String name;
    private List<TaxiDriver> drivers;

    public DispatchOperator(String name, List<TaxiDriver> drivers) {
        this.name = name;
        this.drivers = drivers;
    }

    public void eat() {
        System.out.println("Operator " + name + " eats");
    }

    public void dispatch(String location) {
        getBestAvailableTaxi(location).goToAddress(location);
    }

    private TaxiDriver getBestAvailableTaxi(String location) {
        final int FIRST_POSITION = 0;
        return drivers.get(FIRST_POSITION);
    }

}
