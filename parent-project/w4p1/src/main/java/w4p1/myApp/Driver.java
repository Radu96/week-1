package w4p1.myApp;

public interface Driver {
    String getCurrentLocation();
    void goToAddress(String address);
}
