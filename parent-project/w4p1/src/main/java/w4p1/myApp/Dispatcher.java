package w4p1.myApp;

public interface Dispatcher {
    void dispatch(String location);
}
