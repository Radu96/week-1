package org.myApp.w9p3;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GenericPriorityQueueTest {

    private GenericPriorityQueue <Integer> integerQueue1;
    private GenericPriorityQueue <Integer> integerQueue2;
    private GenericPriorityQueue <String> stringQueue1;
    private GenericPriorityQueue <String> stringQueue2;

    @Before
    public void setUp() {
        integerQueue1 = new GenericPriorityQueue<>();
        integerQueue2 = new GenericPriorityQueue<>();
        stringQueue1 = new GenericPriorityQueue<>();
        stringQueue2 = new GenericPriorityQueue<>();

        integerQueue1.insert(147);
        integerQueue1.insert(-5428);
        integerQueue1.insert(9067);
        integerQueue1.insert(-832);

        integerQueue2.insert(0);
        integerQueue2.insert(649);
        integerQueue2.insert(-327);
        integerQueue2.insert(5134);

        stringQueue1.insert("abc");
        stringQueue1.insert("ABC");

        stringQueue2.insert("xyz");
        stringQueue2.insert("XYZ");
    }

    @Test(expected = IllegalStateException.class)
    public void testForOverflowShouldThrowException() {
        int maximumSize = 10;
        int startNumber = 0;
        int endNumber = 20;
        int insertElement = 100;
        GenericPriorityQueue <Integer> integerQueue = new GenericPriorityQueue<>(maximumSize);

        for(int i = startNumber; i < endNumber; i++)
            integerQueue.insert(insertElement);
    }

    @Test(expected = IllegalStateException.class)
    public void testForEmptyQueueShouldThrowException() {
        GenericPriorityQueue <String> integerQueue = new GenericPriorityQueue<>();

        integerQueue.remove();
    }

    @Test
    public void testForComparingIntegerQueues() {
        int expectedResult = 1;
        int result = integerQueue1.compareTo(integerQueue2);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testForComparingStringQueues() {
        int result = stringQueue2.compareTo(stringQueue1);
        boolean expectedCondition = result > 0;
        
        assertTrue(expectedCondition);
    }

    @Test
    public void testForSortedListWithIntegerQueue() {
        GenericPriorityQueue <Integer> integerQueue = new GenericPriorityQueue<>();
        List <Integer> integerList = Arrays.asList(812, -613, 12, 5214, -3875, 0, 812, 100);
        List <Integer> expectedIntegerList = Arrays.asList(5214, 812, 812, 100, 12, 0, -613, -3875);

        integerList = integerQueue.sort(integerList);

        assertEquals(expectedIntegerList, integerList);
    }

    @Test
    public void testForSortedListWithStringQueue() {
        GenericPriorityQueue <String> stringQueue = new GenericPriorityQueue<>();
        List <String> stringList = Arrays.asList("house", "dog", "Car", "water", "123", "Plane", "random");
        List <String> expectedStringList = Arrays.asList("water", "random", "house", "dog", "Plane", "Car", "123");

        stringList = stringQueue.sort(stringList);

        assertEquals(expectedStringList, stringList);
    }

    @Test
    public void testForHeadOfTheQueue() {
        int expectedHead = 5134;
        int head = integerQueue2.head();

        assertEquals(expectedHead, head);
    }

    @Test
    public void testForClearMethod() {
        int queueSize;
        int expectedSize = 0;
        GenericPriorityQueue <String> stringQueue = new GenericPriorityQueue<>();

        stringQueue.insert("string1");
        stringQueue.insert("string2");
        stringQueue.insert("string3");
        stringQueue.insert("string4");

        stringQueue.clear();

        queueSize = stringQueue.getList().size();

        assertEquals(expectedSize, queueSize);
    }
}
