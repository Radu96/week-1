package org.myApp.w9p3;

import java.util.ArrayList;
import java.util.List;

public class GenericPriorityQueue <E extends Comparable<? super E>> implements Comparable <GenericPriorityQueue<E>> {
    private List <E> list;
    private static final int DEFAULT_MAXIMUM_SIZE = 15000;
    private static final int FIRST_INDEX = 0;
    private int specifiedMaximumSize = -1;

    public GenericPriorityQueue() {
        list = new ArrayList<>();
    }

    public GenericPriorityQueue(int maxSize) {
        list = new ArrayList<>();
        specifiedMaximumSize = maxSize;
    }

    public void insert(E e) {
        if(list.size() == DEFAULT_MAXIMUM_SIZE || list.size() == specifiedMaximumSize) {
            throw new IllegalStateException("Maximum size reached");
        }

        int lastIndex = list.size() - 1;

        if(list.isEmpty()) {
            list.add(e);
        }
        else if(e.compareTo(list.get(FIRST_INDEX)) > 0 || e.compareTo(list.get(FIRST_INDEX)) == 0) {
            list.add(FIRST_INDEX, e);
        }
        else if(e.compareTo(list.get(lastIndex)) < 0) {
            list.add(e);
        }
        else {
            insertElementInOLogNTime(e);
        }

    }

    public E remove() {
        if(list.isEmpty()) {
            throw new IllegalStateException("Priority queue is empty");
        }

        E largestElement = head();

        list.remove(largestElement);

        return largestElement;
    }

    public void clear() {
        if(!list.isEmpty()) {
            list.clear();
        }
    }

    public E head() {
        if(list.isEmpty()) {
            throw new IllegalStateException("Priority queue is empty");
        }

        return list.get(FIRST_INDEX);
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public List<E> sort(List<E> list) {
        List <E> sortedList = new ArrayList<>();

        for(E object : list) {
            insert(object);
        }

        while(!isEmpty()) {
            sortedList.add(remove());
        }

        return sortedList;
    }

    @Override
    public int compareTo(GenericPriorityQueue<E> genericPriorityQueue) {
        E headOfTheQueue = head();

        return headOfTheQueue.compareTo(genericPriorityQueue.head());
    }

    public List<E> getList() {
        return list;
    }

    private void insertElementInOLogNTime(E e) {
        int half = 2;
        int index = list.size() / half;

        if(e.compareTo(list.get(index)) == 0) {
            list.add(index, e);
        }
        else if(e.compareTo(list.get(index)) > 0) {
                while (e.compareTo(list.get(index)) > 0) {
                    index--;
                }

                index++;
                list.add(index, e);
            }
        else {
            while(e.compareTo(list.get(index)) < 0) {
                index++;
            }

            list.add(index, e);
        }
    }
}
