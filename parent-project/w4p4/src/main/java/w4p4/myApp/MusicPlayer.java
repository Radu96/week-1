package w4p4.myApp;

import java.util.List;

public class MusicPlayer {

    protected List <Song> songs;
    protected List <Playlist> playlists;

    public MusicPlayer(List <Song> songs) {
        this.songs = songs;
    }

    public void playSong(int songPosition) {

        System.out.println("A music player plays \"" + songs.get(songPosition).getName() +
                "\" by " + songs.get(songPosition).getArtist() + " with a duration of " +
                songs.get(songPosition).getDuration());
    }
}
