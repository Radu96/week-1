package w4p4.myApp;

import java.util.List;

public class Band {

    private String name;
    private List <Song> songs;
    private List <Guitarist> guitarists;
    private Drummer drummer;

    public Band(String name, List <Song> songs, List <Guitarist> guitarists, Drummer drummer) {
        this.name = name;
        this.songs = songs;
        this.guitarists = guitarists;
        this.drummer = drummer;
    }

    public void sing(int songPosition) {
        System.out.println("Band " + name + " sings \"" + songs.get(songPosition).getName() + "\"" +
                " with a duration of " + songs.get(songPosition).getDuration());
    }

    public String getMembers() {
        final int FIRST_POSITION = 0;
        final int SECOND_POSITION = 1;
        final int THIRD_POSITION = 2;

        return "Members of the band " + name + " are: \n" +
                guitarists.get(FIRST_POSITION).getName() + ": Lead guitarist, " +
                guitarists.get(SECOND_POSITION).getName() + ": Rhythm guitarist, " +
                guitarists.get(THIRD_POSITION).getName() + ": Bass guitarist, " +
                drummer.getName() + ": Drummer";
    }
}
