package w4p4.myApp;

public class Drummer {

    private Drumset drumset;
    private String name;

    public Drummer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
