package w4p4.myApp;

public class Guitarist {

    private Guitar guitar;
    private String name;
    private Type type;

    public enum Type {
        LEAD_GUITARIST,
        RHYTHM_GUITARIST,
        BASS_GUITARIST
    }

    public Guitarist(String name, Type type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

}
