package w4p4.myApp;

import java.util.List;

public class Ipod extends MusicPlayer {

    public Ipod(List<Song> songs) {
        super(songs);
    }

    @Override
    public void playSong(int songPosition) {

        System.out.println("iPod plays \"" + songs.get(songPosition).getName() +
                "\" by " + songs.get(songPosition).getArtist() + " with a duration of " +
                songs.get(songPosition).getDuration());
    }
}
