package w4p4.main;

import w4p4.myApp.*;

import java.util.ArrayList;
import java.util.List;

public class App
{
    public static void main( String[] args )
    {
        final int FIRST_POSITION = 0;
        final int SECOND_POSITION = 1;

        List <Song> songs = new ArrayList<>();
        Song firstSong = new Song("Livin' on a prayer", "Bon Jovi", "4:10");
        Song secondSong = new Song("It's my life", "Bon Jovi", "3:45");

        songs.add(firstSong);
        songs.add(secondSong);

        List <Guitarist> guitarists = new ArrayList<>();
        Guitarist leadGuitarist = new LeadGuitarist("Paul", Guitarist.Type.LEAD_GUITARIST);
        Guitarist rhythmGuitarist = new RhythmGuitarist("Kevin", Guitarist.Type.RHYTHM_GUITARIST);
        Guitarist bassGuitarist = new BassGuitarist("Ronald", Guitarist.Type.BASS_GUITARIST);
        Drummer drummer = new Drummer("Bruce");

        guitarists.add(leadGuitarist);
        guitarists.add(rhythmGuitarist);
        guitarists.add(bassGuitarist);

        Band band = new Band("Bon Jovi", songs, guitarists, drummer);
        MusicPlayer iPod = new Ipod(songs);

        System.out.println("\n" + band.getMembers());
        band.sing(FIRST_POSITION);
        iPod.playSong(SECOND_POSITION);
    }
}
