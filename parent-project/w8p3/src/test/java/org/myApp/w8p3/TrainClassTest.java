package org.myApp.w8p3;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class TrainClassTest {
    private Set <Train> trains;
    private Train train1;
    private Train train2;
    private Train train3;
    private Train train4;
    private Train train5;

    @Before
    public void setUp() {
        trains = new HashSet<>();

        train1 = new Train(1, "type1", 3);
        train2 = new Train(1, "type1", 3);
        train3 = new Train(2, "type2", 4);
        train4 = new Train(3, "type3", 5);
        train5 = new Train(3, "type3", 5);

        trains.add(train1);
        trains.add(train2);
        trains.add(train3);
        trains.add(train4);
        trains.add(train5);
    }

    @Test
    public void testEqualObjects() {
        Set <Train> expectedTrainList = new HashSet<>();

        expectedTrainList.add(train1);
        expectedTrainList.add(train3);
        expectedTrainList.add(train4);

        assertEquals(expectedTrainList, trains);
    }
}
