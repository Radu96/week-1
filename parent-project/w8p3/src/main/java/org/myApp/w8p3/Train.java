package org.myApp.w8p3;

import java.util.Objects;

public class Train {
    private int trainNumber;
    private String trainType;
    private int numberOfWagons;

    public Train(int trainNumber, String trainType, int numberOfWagons) {
        this.trainNumber = trainNumber;
        this.trainType = trainType;
        this.numberOfWagons = numberOfWagons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Train train = (Train) o;
        return trainNumber == train.trainNumber &&
                numberOfWagons == train.numberOfWagons &&
                Objects.equals(trainType, train.trainType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trainNumber, trainType, numberOfWagons);
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    public String getTrainType() {
        return trainType;
    }

    public int getNumberOfWagons() {
        return numberOfWagons;
    }
}
