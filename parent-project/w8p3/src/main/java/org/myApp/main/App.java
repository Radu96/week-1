package org.myApp.main;

import org.myApp.w8p3.Train;

import java.util.HashSet;
import java.util.Set;

public class App
{
    public static void main( String[] args )
    {
        Set <Train> trains = new HashSet<>();

        trains.add(new Train(1, "type1", 3));
        trains.add(new Train(1, "type1", 3));
        trains.add(new Train(2, "type2", 4));
        trains.add(new Train(3, "type3", 5));
        trains.add(new Train(3, "type3", 5));

        for(Train train : trains)
            System.out.println(train.getTrainNumber() + " " + train.getTrainType() + " " + train.getNumberOfWagons());
    }
}
