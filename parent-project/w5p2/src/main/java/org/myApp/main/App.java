package org.myApp.main;

import org.myApp.w5p2.TitlelizeString;
import org.myApp.w5p2.Titlelizer;

public class App
{
    public static void main( String[] args )
    {
        Titlelizer titlelizer = new TitlelizeString();
        System.out.println(titlelizer.titlelize("Now is the time to act!"));
    }
}
