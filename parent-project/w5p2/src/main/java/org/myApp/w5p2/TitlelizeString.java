package org.myApp.w5p2;

import java.util.Arrays;
import java.util.List;

public class TitlelizeString implements Titlelizer {

    private static List <String> ignoredWords = Arrays.asList("the", "a", "to", "in", "of", "is");
    private static final int FIRST_INDEX = 0;

    @Override
    public String titlelize(String toTitlelize) {
        if(toTitlelize == null)
            throw new IllegalArgumentException();

        if(toTitlelize.isEmpty())
            return toTitlelize;

        String regex = " ";
        String titlelizedString = "", newWord = "";

        String[] tokens = toTitlelize.split(regex);
        final int LAST_TOKEN_INDEX = tokens.length - 1;

        for (int index = 0; index < LAST_TOKEN_INDEX; index++) {
            newWord = capitalizeFirstLetter(tokens[index]);
            titlelizedString = titlelizedString.concat(newWord + " ");
        }

        newWord = capitalizeFirstLetter(tokens[LAST_TOKEN_INDEX]);
        titlelizedString = titlelizedString.concat(newWord);

        return titlelizedString;
    }

    private String capitalizeFirstLetter(String word) {
        if(!ignoredWords.contains(word))
            if(word.charAt(FIRST_INDEX) >= 'a' && word.charAt(FIRST_INDEX) <= 'z') {
                String newWord = "";
                int firstBeginIndex = 0, endIndex = 1, secondBeginIndex = 1;

                newWord = word.substring(firstBeginIndex, endIndex).toUpperCase() + word.substring(secondBeginIndex);

                return newWord;
            }

        return word;
    }
}
