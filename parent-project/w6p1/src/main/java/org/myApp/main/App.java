package org.myApp.main;

import org.myApp.w6p1.OutputEnum;

public class App
{
    public static void main( String[] args )
    {
        OutputEnum outputEnum = new OutputEnum();

        outputEnum.printDayOfTheWeek();
        outputEnum.printMonthOfTheYear();
        outputEnum.printYear();
    }
}
