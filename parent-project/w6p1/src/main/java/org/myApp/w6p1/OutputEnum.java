package org.myApp.w6p1;

import java.util.Calendar;

public class OutputEnum {
    private int day;
    private int month;
    private int year;

    public OutputEnum() {
        Calendar calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_WEEK); // Starts from Sunday, beginning from index 1
        month = calendar.get(Calendar.MONTH); // Begins from index 0
        year = calendar.get(Calendar.YEAR);
    }

    public void printDayOfTheWeek() {
        int firstDay = 1;

        if(day == firstDay) // Sunday
            System.out.println("SUNDAY ");
        else {
            day--;
            for (Day dayOfTheWeek : Day.values())
                if (dayOfTheWeek.getIndex() == day)
                {
                    System.out.print(dayOfTheWeek.name() + " ");
                    break;
                }
        }
    }

    public void printMonthOfTheYear() {
        int incrementByOne = 1;

        for(Month monthOfTheYear : Month.values())
            if(monthOfTheYear.getIndex() == (month + incrementByOne))
            {
                System.out.print(monthOfTheYear.name() + " ");
                break;
            }
    }

    public void printYear() {
        System.out.println(year);
    }
}
