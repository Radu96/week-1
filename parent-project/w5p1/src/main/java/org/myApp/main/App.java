package org.myApp.main;

import org.myApp.w5p1.CustomException;
import org.myApp.w5p1.StringList;

import java.util.List;

public class App
{
    public static void main( String[] args ) throws CustomException {
        StringList stringList = new StringList();
        StringList foreignList = new StringList();

        List <String> record = stringList.getRecord();
        String[] stringArray = {"12", "23", "34", "45"};
        int index = 3;

        for (String stringElement : stringArray)
            stringList.add(stringElement);

        // stringList.add("abc"); // for testing purposes

        foreignList.add("23");
        foreignList.add("45");
        // foreignList.add("12a"); // for testing purposes

        String element = stringList.get(index);

        System.out.println("");

        if(stringList.contains(element))
            System.out.println("The list contains the provided element");
        else System.out.println("The list doesn't contain the provided element");

        if(stringList.containsAll(foreignList))
            System.out.println("The list contains all of the elements of the foreign list");
        else System.out.println("The list doesn't contain all of the elements of the foreign list");

        System.out.println("Record of the operations:");

        for(String string : record)
            System.out.println(string);

    }
}
