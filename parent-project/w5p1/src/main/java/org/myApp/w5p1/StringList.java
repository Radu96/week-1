package org.myApp.w5p1;

import java.util.ArrayList;
import java.util.List;

public class StringList implements org.myApp.w5p1.List<String> {

    private List <Integer> stringList;
    private List <String> record = new ArrayList<>();

    public StringList() {
        stringList = new ArrayList<>();
    }

    @Override
    public void add(String element) throws CustomException {
        if(element == null)
            throw new CustomException("Null");

        Integer number = parseInteger(element);
        stringList.add(number);
        record.add("Method \"add()\" has been called");
    }

    @Override
    public String get(int position) throws CustomException {
        try {
            String stringElement = String.valueOf(stringList.get(position));
            record.add("Method \"get()\" has been called");
            return stringElement;

        } catch (IndexOutOfBoundsException exception) {
            throw new CustomException("Index out of bounds.");
        }
    }

    @Override
    public boolean contains(String element) throws CustomException {
        record.add("Method \"contains()\" has been called");

        return stringList.contains(parseInteger(element));
    }

    @Override
    public boolean containsAll(org.myApp.w5p1.List<String> foreignList) throws CustomException {
        record.add("Method \"containsAll()\" has been called");

        for (int i = 0; i < foreignList.size(); i++)
            if (!stringList.contains(Integer.parseInt(foreignList.get(i))))
                return false;

        return true;

    }

    @Override
    public int size() {
        record.add("Method \"size()\" has been called");
        return stringList.size();
    }

    public List<String> getRecord() {
        return record;
    }

    private Integer parseInteger(String string) throws CustomException {
        try {
            return Integer.parseInt(string);

        } catch(NumberFormatException exception) {
            throw new CustomException("Invalid number.");
        }
    }

}
