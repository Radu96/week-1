package org.myApp.w5p1;

public class CustomException extends Exception {

    public CustomException(String message) {
        super(message);
    }

}
