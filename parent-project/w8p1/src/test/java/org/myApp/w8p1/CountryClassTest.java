package org.myApp.w8p1;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CountryClassTest {
    private Country country1;
    private Country country2;
    private Country country3;
    private Country country4;
    private Country country5;
    private List <Country> countries;

    @Before
    public void setUp() {
        country1 = new Country("Romania", "Bucharest");
        country2 = new Country("Italy", "Rome");
        country3 = new Country("Spain", "Madrid");
        country4 = new Country("France", "Paris");
        country5 = new Country("Germany", "Berlin");

        countries = new ArrayList<>();

        countries.add(country1);
        countries.add(country2);
        countries.add(country3);
        countries.add(country4);
        countries.add(country5);
    }

    @Test
    public void testNameSorting() {
        List <Country> expectedCountriesList = Arrays.asList(country4, country5, country2, country1, country3);

        Collections.sort(countries);

        assertEquals(countries, expectedCountriesList);
    }

    @Test
    public void testCapitalSorting() {
        List <Country> expectedCountriesList = Arrays.asList(country5, country1, country3, country4, country2);

        Collections.sort(countries, new CapitalComparator());

        assertEquals(countries, expectedCountriesList);
    }

    @Test
    public void testBinarySearchForCapital() {
        String capitalName = "Paris";
        int capitalIndex = 0;

        Collections.sort(countries, new CapitalComparator());

        for(int i = 0; i < countries.size(); i++)
            if(capitalName.equalsIgnoreCase(countries.get(i).getCapital())) {
                capitalIndex = i;
                break;
            }

        int expectedCapitalIndex = Collections.binarySearch(countries, country4, new CapitalComparator());

        assertEquals(capitalIndex, expectedCapitalIndex);
    }

}
