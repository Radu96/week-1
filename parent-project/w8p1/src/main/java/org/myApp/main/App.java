package org.myApp.main;

import org.myApp.w8p1.CapitalComparator;
import org.myApp.w8p1.Country;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App
{
    public static void main( String[] args )
    {
        Country country1 = new Country("Romania", "Bucharest");
        Country country2 = new Country("Italy", "Rome");
        Country country3 = new Country("Spain", "Madrid");
        Country country4 = new Country("France", "Paris");
        Country country5 = new Country("Germany", "Berlin");

        List<Country> countries1 = new ArrayList<>();
        List<Country> countries2 = new ArrayList<>();

        countries1.add(country1);
        countries1.add(country2);
        countries1.add(country3);
        countries1.add(country4);
        countries1.add(country5);

        countries2.add(country1);
        countries2.add(country2);
        countries2.add(country3);
        countries2.add(country4);
        countries2.add(country5);

        Collections.sort(countries1);
        Collections.sort(countries2, new CapitalComparator());

        System.out.println("\nCountries sorted by name:");

        for(Country country : countries1)
            System.out.println(country.getName());

        System.out.println("\nCountries sorted by capital:");

        for(Country country : countries2)
            System.out.println(country.getName() + ", " + country.getCapital());
    }
}
