package org.myApp.w8p1;

public class Country implements Comparable<Country> {
    private String name;
    private String capital;

    public Country(String name, String capital) {
        this.name = name;
        this.capital = capital;
    }

    @Override
    public int compareTo(Country country) {
        return this.name.compareToIgnoreCase(country.name);
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }
}
