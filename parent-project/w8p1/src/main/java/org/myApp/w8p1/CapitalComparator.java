package org.myApp.w8p1;

import java.util.Comparator;

public class CapitalComparator implements Comparator<Country> {

    @Override
    public int compare(Country country1, Country country2) {
        return country1.getCapital().compareToIgnoreCase(country2.getCapital());
    }
}
