package w2p1.myApp;

public class Person {
    private String firstName = "";
    private String surname = "";

    public Person(String firstName, String surname) {
        this.firstName = firstName;
        this.surname = surname;
    }

    public Person(String fullName) {
        String[] stringArray = splitFullName(fullName);
        int surnamePosition = stringArray.length - 1;

        firstName = concatFirstName(stringArray);
        surname = stringArray[surnamePosition];
    }

    private String[] splitFullName(String fullName) {
        String regex = " ";
        return fullName.split(regex);
    }

    private String concatFirstName(String[] stringArray) {
        int lastFirstNamePosition = stringArray.length - 2;
        String firstName = "";

        for(int i = 0; i < lastFirstNamePosition; i++)
            firstName = firstName.concat(stringArray[i] + " ");

        firstName = firstName.concat(stringArray[lastFirstNamePosition]);
        return firstName;
    }

    @Override
    public String toString() {
        return "First name: " + firstName + ", surname: " + surname;
    }

}
