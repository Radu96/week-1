package w2p1.main;

import w2p1.myApp.Person;

public class App 
{
    public static void main( String[] args )
    {
        Person firstPerson = new Person("Radu", "Rijnita");
        Person secondPerson = new Person("Andrei Marius", "Ionescu");
        Person thirdPerson = new Person("Ioana Daniela Maria Popescu");

        System.out.println("First object: " + firstPerson);
        System.out.println("Second object: " + secondPerson);
        System.out.println("Third object: " + thirdPerson);
    }
}
