package w1p1.myApp;

import java.util.Objects;

public class Person
{
    private String firstName, lastName;
    private String dob, dod;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setDod(String dod) {
        this.dod = dod;
    }

    @Override
    public String toString() {

        if(this.dod != null)
            return this.firstName + this.lastName + "(" + this.dob + "-" + this.dod + ")";

        return this.firstName + this.lastName + "(" + this.dob + ")";
    }

    @Override
    public boolean equals(Object object) {
        if (this == object)
            return true;

        if (object == null || getClass() != object.getClass())
            return false;

        Person person = (Person) object;
        return Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(dob, person.dob) &&
                Objects.equals(dod, person.dod);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, dob, dod);
    }
}