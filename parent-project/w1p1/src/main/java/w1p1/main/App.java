package w1p1.main;

import w1p1.myApp.Parser;
import w1p1.myApp.Person;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

public class App
{
    private static final int FIRST_NAME_POSITION = 0;
    private static final int LAST_NAME_POSITION = 1;
    private static final int DATE_OF_BIRTH_POSITION = 2;
    private static final int DATE_OF_DEATH_POSITION = 3;
    private static final int TOKENS_LENGTH = 4;

    public static void main( String[] args )
    {
        String[] result = null;
        String[] tokens;
        Set<Person> persons = new HashSet<>();
        Person person;

        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream inputStream = classloader.getResourceAsStream("W1P1input.txt");
            result = Parser.readLinesFromTextFile(inputStream);
        }
        catch(NullPointerException ex) {
            System.err.println("Error: File not found!");
            System.exit(-1);
        }
        catch(IOException ex) {
            System.err.println("Error: Input/output error!");
            System.exit(-1);
        }

        for (String line : result) {
            tokens = line.split(",");

            person = new Person();
            person.setFirstName(tokens[FIRST_NAME_POSITION]);
            person.setLastName(tokens[LAST_NAME_POSITION]);
            person.setDob(tokens[DATE_OF_BIRTH_POSITION]);

            if (tokens.length == TOKENS_LENGTH)
                person.setDod(tokens[DATE_OF_DEATH_POSITION]);

            persons.add(person);
        }

        for (Person value : persons)
            System.out.println(value.toString());

        //System.out.println(persons.size());
    }
}
